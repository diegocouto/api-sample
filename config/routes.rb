Inscricoes::Application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :projects, only: [:show]
    end
  end
end