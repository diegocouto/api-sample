module Api
  module V1
    class ProjectsController < ApiController
      before_filter :restrict_access

      ##
      # Retorna as atividades para o dado projeto de extensão.
      #
      # GET /api/v1/projects/:numero_do_protocolo
      #
      # params:
      #   token_key - API token.
      #
      # = Exemplos
      #
      #   resp = conn.get("/api/v1/projects/20140101.json", "token" => "dcbb7b36acd4438d07abafb8e28605a4")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => { "13": {
      #                 "activity_code": 13,
      #                 "activity_name": "Aprimoramento EAD",
      #                 "edition_name": "Edição 2015/2",
      #                 "participation_type": "Participante"
      #              }
      #      }
      #
      #   resp = conn.get("/api/v1/projects/20140520.json", "token" => "dcbb7b36acd4438d07abafb8e28605a4")
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message": "Projeto não encontrado."}
      #
      def show
        activities = Activity.where project_protocol: params[:id].gsub(/(.{4})(?!$)/, '\1.')
        activities_list = {}

        return render json: {message: 'Projeto não encontrado.'}, status: :not_found unless activities.present?

        activities.each do |activity|
          activities_list[activity.id] = {
              activity_code: activity.id,
              activity_name: activity.name,
              edition_name: activity.edition_name,
              participation_type: activity.participation_type.name
          }
        end

        render json: activities_list, status: :ok
      end
    end
  end
end