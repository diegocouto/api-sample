class ApiController < ActionController::Base
  protect_from_forgery with: :null_session

  def restrict_access
    unless Token.exists? key: params[:token_key]
      render json: {message: 'API Token Inválido.'}, status: 401
    end
  end

  def restrict_access_to_system
    unless Token.exists? service: :system, key: params[:token_key]
      render json: {message: 'Acesso Negado.'}, status: 401
    end
  end
end